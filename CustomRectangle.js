class CustomRectangle {
  constructor(p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y) {
    this.p1x = p1x;
    this.p1y = p1y;
    this.p2x = p2x;
    this.p2y = p2y;
    this.p3x = p3x;
    this.p3y = p3y;
    this.p4x = p4x;
    this.p4y = p4y;
    this.height, this.width;
  }

  findSideLengths() {
    let height = 0;
    let width = 0;

    if (this.p1x === this.p2x) {
      //the second point is vertical from the start point
      height = this.p1y - this.p2y;
      width = this.p3x - this.p2x;
    } else if (this.p1y === this.p2y) {
      //the second point is horisontal from the start point
      length = this.p1x - this.p2x;
      width = this.p3x - this.p2x;
    } else {
      //Error, the point is not valid
      //alert("Error, please enter a valid rectangle");
      console.log("Not a valid rectangle");
    }
    this.height = height;
    this.width = width;
  }

  Render() {
    this.findSideLengths();
    //rectangle(this.p1x, this.p1y, this.p2x, this.p2y, this.p3x, this.p3y, this.p4x, this.p4y);
    //This won't work... because:
    //
    rect(this.p1x, this.p1y, this.width, this.height);
  }
}
