class CustomTriangle {
  constructor(p1x, p1y, p2x, p2y, p3x, p3y) {
    this.p1x = p1x;
    this.p1y = p1y;
    this.p2x = p2x;
    this.p2y = p2y;
    this.p3x = p3x;
    this.p3y = p3y;
  }

  Render() {
    triangle(this.p1x, this.p1y, this.p2x, this.p2y, this.p3x, this.p3y);
  }
}
