

var x,
  y,
  gridSpace = 10;
var sketch = [];
var tempShapeArray = [];
var closestNode = [0, 0];
var lastClickedNode;
var scope;


//Allows user to clear sketch on canvas
var clearSketch = document
  .querySelector("#clear-sketch")
  .addEventListener("submit", e => {
    tempShapeArray = [];
    closestNode = [0, 0];
  });

function setup() {
  background(155);
  var canvasBase = createCanvas(1250, 1250);
  canvasBase.parent("#sketch-holder");
}

function draw() {
  gridSetup();
  stroke(0, 0, 0);
  fill(0);
  textSize(32);
  showMouseLocation();
  noFill();

  drawTempShape();
  drawShapes();

  if (lastClickedNode) {
    line(
      lastClickedNode[0],
      lastClickedNode[1],
      closestNode[0],
      closestNode[1]
    );
  }

  highlightNearestNode();
}

function drawShapes() {
  strokeWeight(2);
  for (var a = 0; a < sketch.length; a++) {
    if(sketch[a])
    sketch[a].Render();
  }
}

function drawTempShape() {
  for (var a = 0; a < tempShapeArray.length; a++) {
    if (a == 0) {
      continue;
    }
    if (a == tempShapeArray.length - 1) {
      lastPoint = [tempShapeArray[a][0], tempShapeArray[a][1]];
    }

    stroke(0);
    strokeWeight(4);
    line(
      tempShapeArray[a - 1][0],
      tempShapeArray[a - 1][1],
      tempShapeArray[a][0],
      tempShapeArray[a][1]
    );
  }
}

function highlightNearestNode() {
  let nodeCircleSize = 15;

  if (gridSpace === 10) {
    nodeCircleSize = 8;
  }
  var x = Math.round(mouseX / gridSpace) * gridSpace;
  var y = Math.round(mouseY / gridSpace) * gridSpace;
  stroke(0);
  ellipse(x, y, nodeCircleSize, nodeCircleSize);
  closestNode = [x, y];
}

function withinCanvasBounds() {
  var posX = int(mouseX);
  var posY = int(mouseY);

  return (
    posX <= width &&
    posY <= height &&
    Math.sign(posY) != -1 &&
    Math.sign(posX) != -1
  );
}

function mousePressed() {
  if (withinCanvasBounds()) {
    // adding the new point to tempArray
    tempShapeArray.push(closestNode);
    lastClickedNode = closestNode;

    // Checking if shape is closed
    if (tempShapeArray.length > 1) {
      if (
        tempShapeArray[0][0] == lastClickedNode[0] &&
        tempShapeArray[0][1] == lastClickedNode[1]
      ) {
        console.log("NEW SHAPE");
        createShape(tempShapeArray);
        tempShapeArray = [];
        lastClickedNode = null;
        console.log('we are you')
      }
    }
  }
}

function createShape(tempShape) {
  console.log("tempShape: ", tempShape);
  switch (tempShape.length) {
    case 4:
      console.log("triangle");
      sketch.push(
        new CustomTriangle(
          tempShape[0][0],
          tempShape[0][1],
          tempShape[1][0],
          tempShape[1][1],
          tempShape[2][0],
          tempShape[2][1]
        )
      );
      break;
    case 5:
      console.log("rect");
      sketch.push(new CustomRectangle(
        tempShape[0][0],
        tempShape[0][1],
        tempShape[1][0],
        tempShape[1][1],
        tempShape[2][0],
        tempShape[2][1],
        tempShape[3][0],
        tempShape[3][1]

      ));
      break;
    default:
      break;
  }
}

//Defines the grid spacing of the canvas
function gridSetup() {
  background(224);
  for (var a = gridSpace; a <= width - gridSpace; a = a + gridSpace) {
    stroke(34, 139, 34); //forest green
    strokeWeight(1);

    //Sets major Y lines
    var darkLineY = a % 50;
    if (darkLineY == 0) {
      strokeWeight(2);
      line(a, 0, a, height);
    } else {
      strokeWeight(0.5);
      line(a, 0, a, height);
    }
  }

  for (var b = gridSpace; b <= height - gridSpace; b = b + gridSpace) {
    stroke(34, 139, 34); //forest green
    strokeWeight(1);

    //Sets major X lines
    var darkLineX = b % 50;
    if (darkLineX == 0) {
      strokeWeight(2);
      line(0, b, width, b);
    } else {
      strokeWeight(0.5);
      line(0, b, width, b);
    }
  }
}

function showMouseLocation() {
  fill(255);
  rect(5, height - 70, 230, 65);
  fill(0);
  text("Mouse X: " + int(mouseX), 10, height - 10);
  text("Mouse Y: " + int(mouseY), 10, height - 40);
}
